// dev mode?

var devBuild = process.env.NODE_ENV !== "production"

// modules

const gulp = require("gulp"),
  newer = require("gulp-newer"),
  imagemin = require("gulp-imagemin"),
  noop = require("gulp-noop"),
  concat = require("gulp-concat"),
  terser = devBuild ? null : require("gulp-terser"),
  stripdebug = devBuild ? null : require("gulp-strip-debug"),
  sourcemaps = devBuild ? require("gulp-sourcemaps") : null,
  sass = require("gulp-sass"),
  postcss = require("gulp-postcss"),
  assets = require("postcss-assets"),
  autoprefixer = require("autoprefixer"),
  mqpacker = require("css-mqpacker"),
  cssnano = require("cssnano"),
  size = require("gulp-size"),
  pug = require("gulp-pug"),
  browserSync = require("browser-sync").create(),
  babel = require("gulp-babel")

// folders

const src = "src/",
  dev = "dev/",
  build = "build/"

// pug processing

function pugToHtml() {
  return gulp
    .src(src + "views/*.pug")
    .pipe(newer(build))
    .pipe(
      pug({
        pretty: true
      })
    )
    .pipe(gulp.dest(build))
    .pipe(browserSync.stream())
}

// image processing

function images() {
  const out = build + "images/"

  return gulp
    .src(src + "images/**/*.*")
    .pipe(
      size({
        title: "Image size: ",
        pretty: true
      })
    )
    .pipe(newer(out))
    .pipe(imagemin({ optimizationLevel: 5 }))
    .pipe(gulp.dest(out))
    .pipe(browserSync.stream())
}

// CSS processing

function css() {
  return gulp
    .src(src + "scss/styles.scss")
    .pipe(sourcemaps ? sourcemaps.init() : noop())
    .pipe(
      sass({
        outputStyle: "nested",
        imagePath: "/images/",
        precision: 3,
        errorLogToConsole: true
      })
    )
    .on("error", sass.logError)
    .pipe(
      postcss([
        assets({ loadPath: ["images/"] }),
        autoprefixer(),
        mqpacker,
        cssnano
      ])
    )
    .pipe(
      size({
        title: "CSS size:",
        pretty: true
      })
    )
    .pipe(sourcemaps ? sourcemaps.write("./") : noop())
    .pipe(gulp.dest(build + "css/"))
    .pipe(browserSync.stream())
}

// JS processing

function javascript() {
  return gulp
    .src(src + "javascript/**/*.*")
    .pipe(
      size({
        title: "JS size:",
        pretty: true
      })
    )
    .pipe(babel())
    .pipe(sourcemaps ? sourcemaps.init() : noop())
    .pipe(concat("scipts.js"))
    .pipe(stripdebug ? stripdebug() : noop())
    .pipe(terser ? terser() : noop())
    .pipe(sourcemaps ? sourcemaps.write("./") : noop())
    .pipe(gulp.dest(build + "javascript/"))
    .pipe(browserSync.stream())
}

// watch for file changes

function watch(done) {
  // image chages
  gulp.watch(src + "images/**/*", images)

  // css changes
  gulp.watch(src + "scss/**/*", css)

  // js changes
  gulp.watch(src + "javascript/**/*.*", javascript)

  // pug changes
  gulp.watch(src + "views/**/*", pugToHtml)

  done()
}

// browsersync

function serve() {
  if (devBuild) {
    browserSync.init({
      server: {
        baseDir: "./build"
      },
      port: 3000,
      open: false
    })
  }
}

module.exports.pugToHtml = pugToHtml
module.exports.images = images
module.exports.css = gulp.series(images, css)
module.exports.javascript = javascript
module.exports.watch = watch
module.exports.serve = serve

// build task

module.exports.build = gulp.parallel(
  module.exports.pugToHtml,
  module.exports.css,
  module.exports.javascript,
  module.exports.serve
)

// default task

module.exports.default = gulp.parallel(exports.build, exports.watch)
